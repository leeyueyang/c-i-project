#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>

const int redPin = 3;
const int greenPin = 4;
const int bluePin = 5;

//Data Variables setup
String data;
char username[] = "myUsername";
int myTemp = 33;
String t = String(myTemp);


//WiFi Setup
WiFiClient client;
IPAddress server(128, 199, 201, 137);
//byte server[]={ 128 , 199 , 201 , 137 };
char address[] = "candi2017.ddns.net";
char ssid[] = "CnI";       //  your network SSID (name) 
char pass[] = "candi2017";       // your network password
int status = WL_IDLE_STATUS;      // the Wifi radio's status

void setup() {
  // initialize serial:
  Serial.begin(9600);

  // attempt to connect using WPA2 encryption:
  Serial.println("Attempting to connect to WPA network...");
  status = WiFi.begin(ssid, pass);

  // if you're not connected, stop here:
  if ( status != WL_CONNECTED) { 
    Serial.println("Couldn't get a wifi connection");
    while(true);
  } 
  // if you are connected, print out info about the connection:
  else {
    Serial.println("Connected to network");
  }
}

void loop() {

  // temperature sensor reading here
  pinMode(greenPin, OUTPUT);
  pinMode(redPin, OUTPUT);
  digitalWrite(greenPin, LOW);  digitalWrite(redPin, LOW);

  // data to be sent
  data = "temp=" + t + "&user=" + username;
  Serial.println("Parsed data");

  Serial.println("Beginning our connection to the server");
  if (client.connect(server,80)){ // server address
    Serial.println("Connected to server");  digitalWrite(greenPin, HIGH);

    client.print("GET /addproto3.php?");
    //client.print("temp=10000&user=final5"); // send our data
    client.print(data);
    client.println(" HTTP/1.1");
    
    client.println("Host: candi2017.ddns.net"); //server address also
    //client.println("Accept: text/html");
    //client.println("User-Agent: Arduino/1.0");

    //client.println("Connection: close");
    client.println();
    client.println();

    delay(5000);   //YOU MUST ADD THIS OR ELSE WILL NOT WORK
    client.stop();

    delay(10000);
    /*
    client.println("Content-Type: application/x-www-form-urlencoded; charset=UTF-8");
    client.print("Content-Length: ");
    client.println(data.length());
    client.println(data);
    */
    
    Serial.println("Data sent");

    /*
    if (client.connected()){
      client.stop();
      Serial.println("Connection to server closed");
      delay(5000);
    }*/
    

    Serial.println("Beginning our data interval delay");
    //delay(300000);  //five minute delay

  }
  else{
    digitalWrite(redPin, HIGH);
    Serial.println("\nServer connection failed!\n"); delay(2000);

    client.flush();
    client.stop();
    Serial.println("Connection to server closed");
  
  }
}

void confirmed(){
  for(int i =0; i < 2; i++){
    digitalWrite(greenPin, HIGH);
    delay(100);                  
    digitalWrite(greenPin, LOW);
    delay(100);
  }
}

void error(){
  for(int i =0; i < 2; i++){
    digitalWrite(redPin, HIGH);
    delay(100);                  
    digitalWrite(redPin, LOW);
    delay(100);
  }
}

